var async = require('async'), keystone = require('keystone');

var Post = keystone.list('Post');

/**
 * List Posts
 */
exports.list = function(req, res) {
    Post.model.find(function(err, items) {

        if (err) return res.json('database error', err);

        res.json({
            posts: items
        });

    });
}